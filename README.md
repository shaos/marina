This README file was initially copied from

https://github.com/efabless/caravel_board/tree/main/firmware/gf180

and updated by Shaos to include correct information

# Caravel Board Firmware for the VexRiscv Processor

This repo provides firmware examples, flash programming and diagnostic tools for testing
Open MPW and chipIgnite projects using Caravel on the open-source GF180mcu technology node.
It also provides schematics, layout and gerber files for PCB evaluation and breakout boards.

## Firmware

You will need python 3.6 or later.  

To program Caravel, connect the evaluation board using a USB micro B connector.

```bash
pip3 install pyftdi

cd blink

make clean flash
```

### Install Toolchain for Compiling Code

#### For Mac

https://github.com/riscv/homebrew-riscv

#### For Linux

https://github.com/riscv/riscv-gnu-toolchain

### Diagnostics

Makefiles in the firmware project directories use 

> util/caravel_hkflash.py

to program the flash on the board through Caravel's housekeeping SPI interface.

> util/caravel_hkdebug.py

provides menu-driven debug through the housekeeping SPI interface for Caravel.

## Hardware

The current evaluation board from efabless for 5V Caravel can be found at 

https://github.com/efabless/caravel_board/tree/main/hardware/development/caravel-5V-dev-v6-M.2

> _hardware/caravel-5V-dev-v6-M.2

- The clock is driven by X1 with a frequency of 10MHz. To drive the clock with custom frequnecy, disable X1 through J6 and use the external pin for `xclk`

- To use serial console insert jumper switch J2 (Enable UART). This jumper should be removed to falsh the firmware.

The most updated breakout board for Caravel in QFN package can be found at 

https://github.com/efabless/caravel_board/tree/main/hardware/breakout/caravel-M.2-card-QFN

> _hardware/caravel_breakout_QFN
