#include <defs.h>
#include <stub.h>
void println(){putchar('\n');}
void print_bin(int byte)
{
  int i;
  for(i=7;i>=0;i--)
  {
    if(byte&(1<<i)) putchar('1');
    else putchar('0');
    if(i==4) putchar(' ');
  }
}

// --------------------------------------------------------
// Firmware routines
// --------------------------------------------------------

void configure_io()
{

//  ======= Useful GPIO mode values =============

//      GPIO_MODE_MGMT_STD_INPUT_NOPULL
//      GPIO_MODE_MGMT_STD_INPUT_PULLDOWN
//      GPIO_MODE_MGMT_STD_INPUT_PULLUP
//      GPIO_MODE_MGMT_STD_OUTPUT
//      GPIO_MODE_MGMT_STD_BIDIRECTIONAL
//      GPIO_MODE_MGMT_STD_ANALOG

//      GPIO_MODE_USER_STD_INPUT_NOPULL
//      GPIO_MODE_USER_STD_INPUT_PULLDOWN
//      GPIO_MODE_USER_STD_INPUT_PULLUP
//      GPIO_MODE_USER_STD_OUTPUT
//      GPIO_MODE_USER_STD_BIDIRECTIONAL
//      GPIO_MODE_USER_STD_ANALOG

#define GPIO_MODE_MGMT_CONTROLLED_IO 0x00F
#define GPIO_MODE_USER_CONTROLLED_IO 0x00E

//  ======= set each IO to the desired configuration =============

    //  GPIO 0 is turned off to prevent toggling the debug pin; For debug, make this an output and
    //  drive it externally to ground.

    reg_mprj_io_0 = GPIO_MODE_MGMT_STD_INPUT_NOPULL;

    // Changing configuration for IO[1-4] will interfere with programming flash. if you change them,
    // You may need to hold reset while powering up the board and initiating flash to keep the process
    // configuring these IO from their default values.

    reg_mprj_io_1 = GPIO_MODE_MGMT_STD_OUTPUT;
    reg_mprj_io_2 = GPIO_MODE_MGMT_STD_INPUT_NOPULL;
    reg_mprj_io_3 = GPIO_MODE_MGMT_STD_INPUT_NOPULL;
    reg_mprj_io_4 = GPIO_MODE_MGMT_STD_INPUT_NOPULL;

    // -------------------------------------------

    reg_mprj_io_5 = GPIO_MODE_MGMT_STD_INPUT_NOPULL;     // UART Rx
    reg_mprj_io_6 = GPIO_MODE_MGMT_STD_OUTPUT;           // UART Tx
    reg_mprj_io_7 = GPIO_MODE_MGMT_STD_OUTPUT;
// user inputs (tiny_silicon_1)
    reg_mprj_io_8 = GPIO_MODE_MGMT_CONTROLLED_IO;
    reg_mprj_io_9 = GPIO_MODE_MGMT_CONTROLLED_IO;
    reg_mprj_io_10 = GPIO_MODE_MGMT_CONTROLLED_IO;
    reg_mprj_io_11 = GPIO_MODE_MGMT_CONTROLLED_IO;
    reg_mprj_io_12 = GPIO_MODE_MGMT_CONTROLLED_IO;
    reg_mprj_io_13 = GPIO_MODE_MGMT_CONTROLLED_IO;
    reg_mprj_io_14 = GPIO_MODE_MGMT_CONTROLLED_IO;
    reg_mprj_io_15 = GPIO_MODE_MGMT_CONTROLLED_IO;
// user outputs (tiny_silicon_1)
    reg_mprj_io_16 = GPIO_MODE_USER_CONTROLLED_IO;
    reg_mprj_io_17 = GPIO_MODE_USER_CONTROLLED_IO;
    reg_mprj_io_18 = GPIO_MODE_USER_CONTROLLED_IO;
    reg_mprj_io_19 = GPIO_MODE_USER_CONTROLLED_IO;
    reg_mprj_io_20 = GPIO_MODE_USER_CONTROLLED_IO;
    reg_mprj_io_21 = GPIO_MODE_USER_CONTROLLED_IO;
    reg_mprj_io_22 = GPIO_MODE_USER_CONTROLLED_IO;
    reg_mprj_io_23 = GPIO_MODE_USER_CONTROLLED_IO;

    reg_mprj_io_24 = GPIO_MODE_USER_STD_OUTPUT;
    reg_mprj_io_25 = GPIO_MODE_USER_STD_OUTPUT;
    reg_mprj_io_26 = GPIO_MODE_USER_STD_OUTPUT;
    reg_mprj_io_27 = GPIO_MODE_USER_STD_OUTPUT;
    reg_mprj_io_28 = GPIO_MODE_USER_STD_OUTPUT;
    reg_mprj_io_29 = GPIO_MODE_USER_STD_OUTPUT;
    reg_mprj_io_30 = GPIO_MODE_USER_STD_OUTPUT;
    reg_mprj_io_31 = GPIO_MODE_USER_STD_OUTPUT;
    reg_mprj_io_32 = GPIO_MODE_USER_STD_OUTPUT;
    reg_mprj_io_33 = GPIO_MODE_USER_STD_OUTPUT;
    reg_mprj_io_34 = GPIO_MODE_USER_STD_OUTPUT;
    reg_mprj_io_35 = GPIO_MODE_USER_STD_OUTPUT;
    reg_mprj_io_36 = GPIO_MODE_USER_STD_OUTPUT;
    reg_mprj_io_37 = GPIO_MODE_USER_STD_OUTPUT;

#if 1
    // Initiate the serial transfer to configure IO
    reg_mprj_xfer = 1;
    while (reg_mprj_xfer == 1);
#endif
}

void delay(const int d)
{

    /* Configure timer for a single-shot countdown */
	reg_timer0_config = 0;
	reg_timer0_data = d;
    reg_timer0_config = 1;

    // Loop, waiting for value to reach zero
   reg_timer0_update = 1;  // latch current value
   while (reg_timer0_value > 0) {
           reg_timer0_update = 1;
   }

}

// inputs for tiny_silicon_1

#define CLK   0x01
#define DATA  0x02
#define CLK2  0x04
#define SET   0x08
#define RESET 0x10
#define NAND1 0x20
#define NAND2 0x40
#define DATA2 0x80

int testvec[] = {
// testing D-trigger
0,
CLK,
0,
DATA,
DATA|CLK,
DATA,
0,
CLK,
0,
DATA,
DATA|CLK,
DATA,
// testing D-trigger with SET/RESET
0,
CLK2,
0,
DATA,
DATA|CLK2,
DATA,
0,
CLK2,
0,
DATA,
DATA|CLK2,
DATA,
DATA|RESET,
DATA,
0,
SET,
0,
// testing RS-trigger built out of NAND-gates
NAND1|NAND2,
NAND1,
NAND1|NAND2,
NAND2,
NAND1|NAND2,
// testing C-gate
NAND1|NAND2|DATA,
NAND1|NAND2|DATA|DATA2,
NAND1|NAND2|DATA,
NAND1|NAND2,
NAND1|NAND2|DATA,
NAND1|NAND2|DATA|DATA2,
NAND1|NAND2|DATA2,
NAND1|NAND2,
-1
};


void main()
{
	int i, j, k;

    reg_gpio_mode1 = 1;
    reg_gpio_mode0 = 0;
    reg_gpio_ien = 1;
    reg_gpio_oeb = 0;

    configure_io();

    reg_uart_enable = 1;

    // Configure All LA probes as inputs to the cpu
	reg_la0_oenb = reg_la0_iena = 0x00000000;    // [31:0]
	reg_la1_oenb = reg_la1_iena = 0x00000000;    // [63:32]
	reg_la2_oenb = reg_la2_iena = 0x00000000;    // [95:64]
	reg_la3_oenb = reg_la3_iena = 0x00000000;    // [127:96]

	// write data to la output
    //	reg_la0_data = 0x00;
    //	reg_la1_data = 0x00;
    //	reg_la2_data = 0x00;
    //	reg_la3_data = 0x00;

    // read data from la input
    //	data0 = reg_la0_data;
    //	data1 = reg_la1_data;
    //	data2 = reg_la2_data;
    //	data3 = reg_la3_data;

    print("Hello World !!\n");

	i = j = 0;
	while (1) {

        reg_gpio_out = 1; // OFF
//        reg_mprj_datah = 0x0000002a;
//        reg_mprj_datal = 0xaaaaaaaa;
//	reg_mprj_datal = i<<8;
	reg_mprj_datal = testvec[j++]<<8;
	if(testvec[j]<0) j = 0;

//		delay(50000);
		delay(5000000);

        reg_gpio_out = 0;  // ON
//        reg_mprj_datah = 0x00000015;
//        reg_mprj_datal = 0x55555555;
	k = reg_mprj_datal;

//		delay(50000);
		delay(5000000);

	print("Counter: ");
	print_dec(i++);
	print(" : ");
	print_hex(k,8);
	print(" : ");
	print_bin(k>>8);
	print(" -> ");
	print_bin(k>>16);
	println();
    }


}

